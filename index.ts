import express from 'express';
import request from 'request';
import bodyParser from 'body-parser';
import cors from 'cors';

const app = express();
const port = 3000;

const key = '2237c8bac6ca4520bb4ea25cedec058d';

let city: string;
let start_date: string;
let end_date: string

let url: string;

let weth;

app.use(cors());
app.use(bodyParser.json());
app.listen(port, () => {
  console.log(`Timezones by location application is running on port ${port}.`);
});

app.post('/weather', async function (req, res) {
  console.log(req.body);
  city = req.body.city;
  start_date = req.body.start_date;
  end_date = req.body.end_date;

  url = `https://api.weatherbit.io/v2.0/forecast/daily?city=${city}&days=3&key=${key}`;

  
  await request(url, function (err, response, body) {
      if(err){
        console.log('error:', err);
      } else {
        console.log('body:', body);
        if (body) {
          weth = JSON.parse(body);
          res.send(weth);
        } else {
          res.send(false);
        }
       
      }
    });
});
